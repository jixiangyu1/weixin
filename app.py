# -*- coding: utf-8 -*-
"""
    :author: Grey Li <withlihui@gmail.com>
    :copyright: (c) 2017 by Grey Li.
    :license: MIT, see LICENSE for more details.
"""
import json
import uuid
import time
import sys
reload(sys)
import random
sys.setdefaultencoding('utf-8')
import pymysql
from lxml import etree
import os
from product.Config import req_url, send_txt, temp_html
from product.Util import formating_time, formating_time_ymd
from flask import Flask, render_template, request, url_for, send_from_directory, redirect, jsonify, session
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from flask_ckeditor import CKEditor, CKEditorField, upload_fail, upload_success
from product.Util import MysqlConnect, MongoConnect
basedir = os.path.abspath(os.path.dirname(__file__))
from product.send_demo import WeChat

app = Flask(__name__, static_url_path='/static')
app.config['CKEDITOR_SERVE_LOCAL'] = True
app.config['CKEDITOR_HEIGHT'] = 400
app.config['CKEDITOR_FILE_UPLOADER'] = 'upload'

app.config['UPLOADED_PATH'] = os.path.join(basedir, 'uploads')
app.config['article'] = os.path.join(basedir, 'templates')
app.secret_key = 'secret string'

ckeditor = CKEditor(app)

# mong_db = momgo_content('weixin')
mongo_connect = MongoConnect()
db = mongo_connect["weixin"]
mong_db = db["weixin_gzh"]


# class UnpermissionError(RuntimeError):
#     pass
#
# @app.errorhandler(UnpermissionError)
# def handle_exception(e):
#
#     return redirect(request.host_url + '/login.html')
#
#
# @app.before_request
# def login_check():
#     # print(request.url)
#     result = urlparse.urlparse(request.url)
#
#     boss_path = result.path
#
#     if boss_path.endswith('.ico'):
#         return
#
#     if boss_path.startswith('/static/'):
#         return
#
#     if boss_path not in ['/login.html', '/login.do']:
#
#         if session_key not in session:
#
#             raise UnpermissionError


@app.route('/')
def welcome():

    return redirect('/login.html')


@app.route('/login.html')
def login_page():
    return render_template('login.html')


@app.route('/login.do', methods=['POST'])
def login_post():
    ok = False

    form = request.form
    username=form['username']
    password=form['password']
    ga = form['ga']
    print(ga)
    print(staff.keys())
    print(username)
    print(password)
    print(users[username])
    if ga == '8643' and username in users.keys() and password == users[username]:
        print("sdgjl")
        return jsonify({'ok': True})
    # else:
    #     return jsonify({'false': "sddg"})
    # result = match(form['username'], form['password'])

    # if result:
    #     ok = True
    #     session[session_key] = {'username': form['username']}
    #     session.permanent = True
        # app.permanent_session_lifetime = timedelta(minutes=30)

    return jsonify({'ok': ok})


def match(username, password):

    pass

    # if employee is None:
    #     return False
    #
    # # if employee.limit_expire:
    # #
    # #     now = datetime.now()
    # #
    # #     if employee.limit_expire > now:
    # #         return False
    #
    # if employee.username == username and employee.password == password:
    #     employee.error_count = 0
    #     employee.limit_expire = None
    #     return True

    # employee.error_count += 1
    #
    # employee.limit_expire = datetime.now() + timedelta(seconds=(employee.error_count * 10))

    # return False



class PostForm(FlaskForm):
    title = StringField('Title')
    author = StringField('author')
    body = CKEditorField('Body', validators=[DataRequired()])
    submit = SubmitField()


@app.route('/index.html', methods=['GET', 'POST'])
def index():

    '''
     sql = "SELECT * FROM weixin_news "
    cursur.execute(sql)
        # sql = "INSERT INTO weixin_news (`title`, `author`, `publish_time`, `content`, `read_num`, `status`) VALUES {}".format((title, author, formating_time(), content, 0, 0))
        # print(sql)
        # cursur.execute(sql)
        # db.commit()
        # print("存储数据库")
        # urls = req_preview(title, author, formating_time_ymd(), body)

    :return:
    '''
    print(mong_db)
    fetchall_list = mong_db.find({})
    data_list = list(fetchall_list)
    print(data_list)
    mode = {"data":data_list}
    return render_template('index.html', datas=mode)

@app.route('/add_article', methods=['GET', 'POST'])
def add_article():
    form = PostForm()
    if form.validate_on_submit():
        title = form.title.data
        author = form.author.data
        body = form.body.data
        print((title))
        print(title,author, body)
        print(body)
        body1 = etree.HTML(body)
        content = etree.tostring(body1, method='html')
        content = str(content).replace("\\n", "").replace("\\r", "")
        content = str(pymysql.escape_string(content)).replace("\\n", "").replace("\\r", "")
        print(content)
        print(title)
        item = {}
        item['title'] = title
        item['author'] = author
        item['id_flag'] = random.randint(0, 10000)
        item['publish_time'] = formating_time()
        item['content'] = content
        item['read_num'] = 0
        item['status'] = 0
        item['timestamp'] = int(time.time())
        print(item)
        article_write =  '''
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Flask-CKEditor Demo</title>
        </head>
        <body>
            <div class="warpper" style="width: 700px; margin: auto">
                <h1>{}</h1>
                <p>{}&nbsp&nbsp&nbsp&nbsp{}</p>
                <div>{}</div>
                <p style=" margin-top: 30px;">阅读 <span id="read_num">{}</span></p>
            </div>

           <script>
                document.getElementById("read_num").textContent = {};
            </script>
         </body>
         </html>

         '''.format(title, author,formating_time_ymd(), body, item['read_num'], "{{ read_nums }}")
        print(article_write)
        print((os.path.join(app.config['UPLOADED_PATH'], "templates")))
        print("{}\{}.html".format((os.path.join(app.config['article'], "templates")), item['timestamp']))
        with open(temp_html.format((os.path.join(app.config['article'])), item['timestamp']), 'w') as f:
            f.write(article_write)
        mong_db.insert(item)

        return render_template("post.html", title=title, author={'authors':author, "publish_time":formating_time_ymd()},  body=body, read_num=0)
    return render_template('add_article.html', form=form)




@app.route('/bianji', methods=['GET', 'POST'])
def yulan_article():
    if request.method == 'GET':
        id = request.args['id']
        print(id)
        fetchall_list = (mong_db.find({"id_flag": int(id)}))
        data_list = list(fetchall_list)[0]

        class PostForm_bianji(FlaskForm):
            title = StringField('Title', default=data_list.get('title'))
            author = StringField('author', default=data_list.get('author'))
            body = CKEditorField('Body', validators=[DataRequired()], default=data_list.get('content'))
            article_id = StringField(default=id)
            submit = SubmitField()
        form_bianji = PostForm_bianji()

        return render_template('bianji.html', form=form_bianji)

    elif request.method == "POST":
        article_id = request.form['article_id']
        title = request.form['article_id']
        author = request.form['author']
        content = request.form['body']
        print(article_id,title, author, content)
        fetchall_list = (mong_db.find({"id_flag": int(article_id)}))
        data_list = list(fetchall_list)[0]
        data_list['title'] = title
        data_list['author'] = author
        data_list['content'] = content
        mong_db.update_one({'id_flag': int(article_id)}, {'$set': data_list})
        return redirect("http://{}/index.html".format(req_url))


@app.route('/preview', methods=['GET', 'POST'])
def preview_fuc():
    if request.method == 'GET':
        id = request.args['id']
        print(id)
        fetchall_list = (mong_db.find({"id_flag": int(id)}))
        data_list = list(fetchall_list)[0]

        print(data_list)
        print(data_list.get('title'))
        print(data_list.get('author'))
        print(data_list.get('content'))
        print(data_list.get('publish_time'))
        publish_times = str(data_list.get('publish_time')).split(" ")[0]
        print(data_list.get('read_num'))
        return render_template("post.html", title=data_list.get('title'), author={'authors':data_list.get('author'), "publish_time":publish_times}, body=data_list.get("content"), read_num=data_list.get("read_num"))

@app.route('/art', methods=['GET', 'POST'])
def oo():
    read_nums = ''
    if request.method == 'GET':
        id = request.args['id']
        print(id)
        fetchall_list = (mong_db.find({"id_flag": int(id)}))
        data_list = list(fetchall_list)[0]

        data_list['read_num'] = int(data_list.get("read_num")) + 1
        # data_list['status'] = 1
        read_nums = data_list['read_num']
        kkkk = str(data_list.get("timestamp")) + ".html"

        print(data_list)
        mong_db.update_one({'id_flag': int(id)}, {'$set': data_list})

        my_wechat = WeChat("", "", "", "", "http://{}/art?id={}".format(req_url, id))
        my_wechat.post_data()
    return render_template(kkkk, read_nums=read_nums)


@app.route('/files/<filename>')
def uploaded_files(filename):
    path = app.config['UPLOADED_PATH']
    return send_from_directory(path, filename)


@app.route('/upload', methods=['POST'])
def upload():
    f = request.files.get('upload')
    extension = f.filename.split('.')[1].lower()
    if extension not in ['jpg', 'gif', 'png', 'jpeg']:
        return upload_fail(message='Image only!')
    f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))
    url = url_for('uploaded_files', filename=f.filename)
    return upload_success(url=url)




if __name__ == '__main__':

    # mong_db.insert({"content": "<html><body><p>&#21834;&#26432;&#21861;&#30340;&#39118;&#26684;&#21644;</p></body></html>", "author": "\u5927\u6cd5\u5e08", "read_num": 0, "publish_time": "2020-04-24 12:06:01", "title": "\u6d4b\u8bd5"})
    # lll()

    from datetime import datetime
    from gevent import pywsgi
    print(('*' * 20 + ' {} start up orderf server ' + '*' * 20).format(datetime.now()))
    app.secret_key = 'Happy_Quant_Game'
    session_key = 'access_token'
    staff = {}
    users = {
    "lilei": "li123",
    "hanmeimei": "han456",
    "xiaoming": "xiao789"
    }

    wsgi_server = pywsgi.WSGIServer(('0.0.0.0', 3080), app)
    wsgi_server.serve_forever()

    app.run(debug=True)

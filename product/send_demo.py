# -*- coding: utf-8 -*-
import datetime
import requests
import json
import os
from Config import send_txt
class WeChat():
    def __init__(self, openid, name, phone, content, req_url):
        self.openid = ["obGFw5taV-hpHMfzPUDt7Aexuk2U", "obGFw5q6nPaJg4tKVPcLHIbFVPdE"]
        self.name = "jiuxaingyu"
        self.phone = "23785792348"
        self.data = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.content = "nihaoye"
        self.req_url = req_url

    def get_token(self):
        # try:
            basedir = os.path.abspath(os.path.dirname(__file__))
            print("{}\\access_token.txt".format(os.path.join(basedir)))
            with open(send_txt.format(os.path.join(basedir)), "r") as f:
                content = f.read()

                data_dict = json.loads(content)
                print("拿到的{}".format(content))
                print("拿到的{}".format(type(content)))
                time = datetime.datetime.strptime(data_dict["time"], '%Y-%m-%d %H:%M:%S')

            # if "":
            #     pass
            if (datetime.datetime.now() - time).seconds < 7000:
                print("未到两小时，从文件读取")
                return data_dict["access_token"]
            else:
                # 超过两小时，重新获取
                print("超过两小时，重新获取")
                payload = {
                    'grant_type': 'client_credential',
                    'appid': 'wx7f8ac891c7f7f826',  # 公众号appid,按自己实际填写
                    'secret': '42f1d948685db9e4437416adf34497de', #待按自己实际填写
                }
                url = "https://api.weixin.qq.com/cgi-bin/token?"

                try:
                    respone = requests.get(url, params=payload, timeout=50)
                    print(respone.text)
                    access_token = respone.json().get("access_token")
                    # content = "{'access_token':" + str(access_token) + ",'time':" + str(
                    #     datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + "}"
                    content = {}
                    content['access_token'] = access_token
                    content['time'] = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

                    # 写入文件
                    with open(send_txt.format(os.path.join(basedir)), "w") as f:
                        f.write(json.dumps(content))

                    print("get_token", access_token)
                    return access_token
                except Exception as e:
                    print(e)
        # except Exception as e:
        #     print("get_token,file", e)

    def post_data(self):
        for open_id in self.openid:
            data = {
                "touser": open_id,
                "template_id": "pJsRNNw8VDIpyps4XzBhtAOFXsriQOdzAcHGCVOF9r4",  # 模板ID
                "url": self.req_url,
                "topcolor": "#FF0000",
                # "miniprogram":{
                #   "appid":"wx67afc56d7f6cfac0",  #待使用上线小程序appid
                #   "path":"pages/reserve/mgr/mgr"
                # },
                "data": {

                    "keyword1": {
                        "value": "PECO 最新资讯",
                        "color": "#173177"
                    },
                    "keyword2": {
                        "value": "每天一版",
                        "color": "#173177"
                    },

                    "remark": {
                        "value": "点击跳转查看最新资讯！",
                        "color": "#173177"
                    }
                }
            }
            json_template = json.dumps(data)
            access_token = self.get_token()
            print("access_token--", access_token)
            url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token
            try:
                respone = requests.post(url, data=json_template, timeout=50)
                # 拿到返回值
                errcode = respone.json().get("errcode")
                print("test--", respone.json())
                if (errcode == 0):
                    print("模板消息发送成功")
                else:
                    print("模板消息发送失败")
            except Exception as e:
                print("test++", e)

# my_wechat=WeChat("","","","",)
# my_wechat.post_data()

# access_token='31_gumCCNUYUkr-Ls1fMj3x0h9b6NhIsg2xO3--otcgaqAQ9d-mx-zHQfmxVOFfLQZRwy2iwe_GnXsfyHZzXXbudbTlDIkNTN_lUWtIbiZnzhMoA5jbddNcCroqfD0y3G08ogKETbViOtv_k8nqDVHbAAARJV' #未认证的订阅号不能获取，也就是没有权限获取粉丝openid
# next_openid=''
# url='https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=%s'%(access_token,next_openid)
# ans=requests.get(url)
# #print(ans.content)
# a=json.loads(ans.content)['data']['openid']
# print (a,len(a))
# for i in a:
#     print(i)
#     urls = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={}&openid={}&lang=zh_CN".format(access_token, i)
#     response1 = requests.get(urls).text
#     print(response1)
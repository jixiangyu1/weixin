#encoding:utf8
from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex


# 如果text不足16位的倍数就用空格补足为16位
def add_to_16(text):
    if len(text.encode('utf-8')) % 16:
        add = 16 - (len(text.encode('utf-8')) % 16)
    else:
        add = 0
    text = text + ('\0' * add)
    return text.encode('utf-8')


# 加密函数
def encrypt(text, key_value, iv):
    key = key_value.encode('utf-8')
    print(len(key))
    mode = AES.MODE_CBC
    iv = iv.encode('utf-8')
    text = add_to_16(text)
    cryptos = AES.new(key, mode, iv)
    cipher_text = cryptos.encrypt(text)
    # 因为AES加密后的字符串不一定是ascii字符集的，输出保存可能存在问题，所以这里转为16进制字符串
    return b2a_hex(cipher_text).decode()


# 解密后，去掉补足的空格用strip() 去掉
def decrypt(text, key_value, iv):
    key = key_value.encode('utf-8')

    iv = iv.encode('utf-8')
    mode = AES.MODE_CBC
    cryptos = AES.new(key, mode, iv)
    plain_text = cryptos.decrypt(a2b_hex(text))
    return bytes.decode(plain_text).rstrip('\0')


if __name__ == '__main__':
    import time
    key_value = "h!123#@!(（）j"
    iv = "Trademark123!@#%"
    time_str = time.time()
    text = "https://tsk-keyword.zy-hq.com/202003042316390809.jpg"
    e = encrypt(text, key_value, iv)  # 加密
    d = decrypt(e, key_value, iv)  # 解密
    print("加密:", e)
    print("解密:", d)
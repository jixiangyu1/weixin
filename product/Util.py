#encoding:utf8
import pymongo

import binascii
import os
from product.Config import MysqlDB, MongoDB
import pymysql
from Env import ENV
import datetime

_Friday = 4
import pymysql

#获取目前的时间转成时间的格式
def formating_time():
    now_time = datetime.datetime.now()
    access_start_str = now_time.strftime('%Y-%m-%d %H:%M:%S')
    return access_start_str

def formating_time_ymd():
    now_time = datetime.datetime.now()
    access_start_str = now_time.strftime('%Y-%m-%d')
    return access_start_str

class AdminUser:

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.error_count = 0
        self.limit_expire = None

def MongoConnect():
    if ENV == "devel":
        pymongo_client = pymongo.MongoClient(host=MongoDB.get("host"), port=int(MongoDB.get("port")))
        db = pymongo_client[MongoDB.get("db")]
        cursur = db[MongoDB.get("table")]
        return cursur
    print(MongoDB)
    pymongo_client = pymongo.MongoClient('mongodb://{}:{}@{}:{}/'.format(MongoDB.get("username"),
                                          MongoDB.get("password"), MongoDB.get("host"), MongoDB.get("port")))
    print("MongoDB: ", MongoDB)
    return pymongo_client

# def momgo_content(collective):
#
#     return __MONGO_CLIENT['quant_db'][collective]


def MysqlConnect():
    # 创建连接
    connection = pymysql.connect(**MysqlDB)

    return connection

# def des_descrypt(s):
#     """
#     DES 解密
#     :param s: 加密后的字符串，16进制
#     :return:  解密后的字符串
#     """
#     KEY = 'carryEXS'
#     secret_key = KEY
#     iv = secret_key
#     k = des(secret_key, CBC, iv, pad=None, padmode=PAD_PKCS5)
#     return k.encrypt(s)
#     # de = k.decrypt(binascii.a2b_hex(s), padmode=PAD_PKCS5)
#     # return de
#
# if __name__ == '__main__':
#     print(des_descrypt("qqq"))


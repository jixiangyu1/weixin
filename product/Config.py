from Env import *

if ENV == 'devel':
    MysqlDB = {
        "host": "localhost",
        "port": 3306,
        "user": "root",
        "password": "mysql",
        "db": "zyhq_crawl",
        "charset": "utf8mb4",
    }

    RedisDB = {
            "host": "localhost",
            "port": 6379,
            "password": "",
            "db": 1
        }

    MongoDB = {
        "host": "localhost",
        "port": "27017",
        "password": "",
        "username": "",
        "db": "zyhq",
        "table": "tables"
    }

    req_url = "127.0.0.1:3080"
    temp_html = "{}\{}.html"
    send_txt = "{}\\access_token.txt"

elif ENV == 'test':
    MysqlDB = {
        "host": "152.136.202.142",
        "port": 3306,
        "user": "root",
        "password": "Trademark123!@#",
        "db": "zyhq_crawl",
        "charset": "utf8mb4",
    }

    RedisDB = {
            "host": "152.136.202.142",
            "port": 20012,
            "password": "Trademark123!@#^^%",
            "db": 1
        }


    MongoDB = {
        "host": "localhost",
        "port": "27017",
        "password": "",
        "username": "",
        "db": "zyhq",
        "table": "tables"
    }

    req_url = "127.0.0.1:3080"

    # MongoDB = {
    #     "host": "152.136.202.142",
    #     # "host": "62.234.154.163",
    #     "port": "20011",
    #     "password": "Trademark123",
    #     "username": "admin",
    #     "db": "zyhq_crawl_v2",
    # }

elif ENV == 'online':
    MysqlDB = {
        "host": "172.21.0.8",
        "port": 3306,
        "user": "zyhq_rw",
        "password": "NpEoTT7SKd&z3A1Q",
        "db": "zyhq_crawl",
        "charset": "utf8mb4",
    }


    RedisDB = {
            "host": "127.0.0.1",
            "port": 20012,
            "password": "$Tr##_a_&d-395+=$",
            "db": 1,
            # "PW": '$Tr##_a_&d-395+=$'
        }

    MongoDB = {
         "host": "172.21.0.2",
         "port": "20011",
         "password": "%24Tr%23%23_a_%26d-395%2B%3D%24",
         "username": "root",
         "db": "weixin",
         "table": "weixin"
     }

    req_url = "62.234.154.105:3080"
    temp_html = "{}/{}.html"
    send_txt = "{}/access_token.txt"
    #MongoDB = {
    #    "host": "62.234.154.163",
    #    "port": "20011",
    #    "password": "%24Tr%23%23_a_%26d-395%2B%3D%24",
    #    "username": "root",
    #    "db": "zyhq_crawl_v2",
    #    "table": "ecommerce_chn_data"
    #}

